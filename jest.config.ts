import type { Config } from "jest";

const config: Config = {
  preset: "ts-jest",
  testEnvironment: "typescript",
  transform: {
    "^.+\\.ts?$": "ts-jest",
  },
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
};

export default config;
