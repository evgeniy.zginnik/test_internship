/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: {
          500: "#5C99FC",
        },
        secondary: {
          500: "#DDE1F3",
        },
        gray: {
          500: "#3B4143",
        },
        white: {
          100: "#fff",
          700: "#F9F9F9",
        },
      },
    },
  },
  plugins: [],
};
