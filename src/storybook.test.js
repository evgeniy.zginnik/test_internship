// import React from "react";
// import initStoryshots from "@storybook/addon-storyshots";
//
// initStoryshots({
//   suite: "MyApp",
//   test: multiSnapshotWithOptions({}),
//   storyKindRegex: /^((?!.*?App).)*$/,
// });
//
// const isFunction = (obj) => !!(obj && obj.constructor && obj.call && obj.apply);
// const optionsOrCallOptions = (opts, story) =>
//   isFunction(opts) ? opts(story) : opts;
//
// function snapshotWithOptions(options = {}) {
//   return ({ story, context, renderTree, snapshotFileName }) => {
//     const result = renderTree(
//       story,
//       context,
//       optionsOrCallOptions(options, story)
//     );
//
//     async function match(tree) {
//       let target = tree;
//       const isReact = story.parameters.framework === "react";
//
//       // await sleep(400); // gives your complex components some time to get into a stable state after rendering
//
//       if (isReact && typeof tree.childAt === "function") {
//         target = tree.childAt(0);
//       }
//       if (isReact && Array.isArray(tree.children)) {
//         [target] = tree.children;
//       }
//
//       if (snapshotFileName) {
//         expect(target).toMatchSpecificSnapshot(snapshotFileName);
//       } else {
//         expect(target).toMatchSnapshot();
//       }
//
//       if (typeof tree.unmount === "function") {
//         tree.unmount();
//       }
//     }
//
//     if (typeof result.then === "function") {
//       return result.then(match).catch(() => match(result));
//     }
//
//     return match(result);
//   };
// }
//
// function multiSnapshotWithOptions(options = {}) {
//   return ({ story, context, renderTree, stories2snapsConverter }) => {
//     const snapshotFileName =
//       stories2snapsConverter.getSnapshotFileName(context);
//     return snapshotWithOptions(options)({
//       story,
//       context,
//       renderTree,
//       snapshotFileName: snapshotFileName.replace(/^src\\|\//, ""), // assumes your source files are in src/**
//     });
//   };
// }

import initStoryshots from "@storybook/addon-storyshots";
initStoryshots();
