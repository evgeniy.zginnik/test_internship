import { ConfigProvider } from "antd";
import { Footer } from "./ui/Footer";

import { Authorization } from "./modules/Authorization/Authorization";

export default function App() {
  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "#5C99FC",
        },
        hashed: false,
      }}
    >
      <Authorization />

      <Footer />
    </ConfigProvider>
  );
}
