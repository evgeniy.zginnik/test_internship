import type { Meta, StoryObj } from "@storybook/react";
import { Button } from "../ui/Button";

const meta = {
  title: "Example/Button",
  component: Button,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  argTypes: {
    label: { control: "text", description: "Text inside button" },
    type: {
      control: "radio",
      options: ["primary", "secondary", "outlined", "filled"],
      description: "Type of button, switches colors",
    },
    size: {
      control: "radio",
      options: ["default", "mini", "smallest"],
      description: "Vertical button's size",
    },
    width: {
      control: "radio",
      options: ["min", "fixed", "fixedSmall", "max"],
      description: "Width of button",
    },
    disabled: { control: "boolean", description: "Is disabled or not" },
    className: { control: "text", description: "Additional tailwind styles" },
  },
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    label: "Primary Button",
  },
};

export const LogIn: Story = {
  args: {
    label: "Log in",
    type: "outlined",
  },
};

export const Google: Story = {
  args: {
    label: "Google",
    type: "filled",
    width: "min",
  },
};

export const Navigation: Story = {
  args: {
    label: "Sell now",
    type: "secondary",
    size: "mini",
    width: "fixedSmall",
  },
};

export const Advanced: Story = {
  args: {
    label: "Advanced",
    type: "filled",
    size: "smallest",
    width: "fixedSmall",
  },
};

export const Disabled: Story = {
  args: {
    label: "Disabled Button",
    type: "primary",
    size: "default",
    width: "fixed",
    disabled: true,
  },
};
