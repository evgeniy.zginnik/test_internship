import type { Meta, StoryObj } from "@storybook/react";
import { SearchIcon, SettingsIcon } from "../../assets/icons";

import { Input } from "../../ui/Input";

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
const meta = {
  title: "Example/Input",
  component: Input,
  // parameters: {
  //   // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
  //   layout: "centered",
  // },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} satisfies Meta<typeof Input>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const InputBase: Story = {
  args: {
    name: "base",
    value: "",
    label: "base",
  },
};

export const InputPassword: Story = {
  args: {
    name: "base",
    value: "",
    password: true,
    label: "password",
  },
};

export const InputWithIcon: Story = {
  args: {
    name: "base",
    value: "",
    label: "inputWithIcon",
  },
  render: (args) => <Input {...args} iconRight={<SearchIcon />} />,
};

export const InputWithLabelIcon: Story = {
  args: {
    name: "base",
    value: "",
    label: "inputWithLabelIcon",
  },
  render: (args) => <Input {...args} iconLabelStart={<SettingsIcon />} />,
};
