import { useFormik } from "formik";
import { Input } from "../../../ui/Input";
import { Button } from "../../../ui/Button";

interface RegistrationFormProps {
  onCloseForm?: () => void;
}

export const RegistrationForm: React.FC<RegistrationFormProps> = () => {
  const { handleSubmit, handleChange, values, errors, setFieldValue } =
    useFormik({
      initialValues: {
        email: "",
        password: "",
        confirmPassword: "",
        agreePolitic: false,
      },
      onSubmit(values) {
        console.log(values);
      },
    });
  return (
    <form
      onSubmit={handleSubmit}
      className="text-gray-500 h-full flex flex-col justify-between"
    >
      <div>
        <div className="mb-6">
          <Input
            name="email"
            onChange={handleChange}
            value={values.email}
            label="Email"
          />
          {errors.email ? errors.email : null}
        </div>
        <div className="mb-[18px]">
          <Input
            name="password"
            onChange={handleChange}
            value={values.password}
            label="Password"
          />
          {errors.password ? errors.password : null}
        </div>
        <div className="mb-[18px]">
          <Input
            name="confirmPassword"
            onChange={handleChange}
            value={values.confirmPassword}
            label="Confirm password"
          />
          {errors.password ? errors.password : null}
        </div>
        <input
          type="checkbox"
          name="checkbox"
          checked={values.agreePolitic}
          onChange={() => setFieldValue("agreePolitic", !values.agreePolitic)}
        />
        I agree with political Trade-a-plane
      </div>

      <Button label="Register" width="max" htmlType="submit" />
    </form>
  );
};
