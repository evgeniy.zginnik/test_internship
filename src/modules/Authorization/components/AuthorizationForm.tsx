import { useFormik } from "formik";
import { Input } from "../../../ui/Input";
import { Button } from "../../../ui/Button";

interface AuthorizationFormProps {
  onRegistrationChose: () => void;
}

export const AuthorizationForm: React.FC<AuthorizationFormProps> = ({
  onRegistrationChose,
}) => {
  const { handleSubmit, handleChange, values, errors } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit(values) {
      console.log(values);
    },
  });
  return (
    <form
      onSubmit={handleSubmit}
      className="text-gray-500 h-full flex flex-col justify-between"
    >
      <div>
        <div className="mb-6">
          <Input
            name="email"
            onChange={handleChange}
            value={values.email}
            label="Email"
          />
          {errors.email ? errors.email : null}
        </div>
        <div className="mb-[18px]">
          <Input
            name="password"
            onChange={handleChange}
            value={values.password}
            label="Password"
          />
          {errors.password ? errors.password : null}
        </div>
        <button className="mb-9">Forgot password?</button>

        <div className="mb-5">
          <Button htmlType="submit" label="Log in" width="max" />
        </div>
        <div className="flex gap-[18px]">
          <Button label="Google" type="filled" />
          <Button label="Facebook" type="filled" />
        </div>
      </div>

      <Button
        label="Register"
        width="max"
        type="filled"
        onClick={onRegistrationChose}
      />
    </form>
  );
};
