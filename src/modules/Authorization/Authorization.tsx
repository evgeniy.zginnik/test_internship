import { useState } from "react";
import { AuthorizationForm } from "./components/AuthorizationForm";
import { RegistrationForm } from "./components/RegistrationForm";
import { Drawer } from "../../ui/Drawer";
import { CloseFormIcon } from "../../assets/icons";

export const Authorization = () => {
  const [isOpen, setIsOpen] = useState(false);

  const handleClose = () => setIsOpen(false);

  const LoginFormWrapper = ({
    onRegistrationChose,
  }: {
    onRegistrationChose: () => void;
  }) => <AuthorizationForm onRegistrationChose={onRegistrationChose} />;

  const RegistrationFromWrapper = ({
    onCloseForm,
  }: {
    onCloseForm: () => void;
  }) => <RegistrationForm onCloseForm={onCloseForm} />;

  return (
    <>
      <button onClick={() => setIsOpen(true)}>show drawer</button>
      <Drawer
        firstChildTitle="Log in"
        secondChildTitle="Registration"
        open={isOpen}
        onClose={handleClose}
        width={413}
        firstChild={LoginFormWrapper}
        secondChild={RegistrationFromWrapper}
        firstChildExtra={
          <CloseFormIcon onClick={handleClose} className="cursor-pointer" />
        }
      />
    </>
  );
};
