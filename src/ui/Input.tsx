import React from "react";
import { Input as InputAntd } from "antd";
import { SizeType } from "antd/es/config-provider/SizeContext";

interface InputProps {
  size?: SizeType;
  label?: string;
  placeholder?: string;
  password?: boolean;
  iconRight?: React.ReactNode;
  iconLabelStart?: React.ReactNode;
  iconLabelEnd?: React.ReactNode;
  name: string;
  value: string | number;
  onChange: (e: React.ChangeEvent) => void;
}

export const Input = ({
  size = "large",
  label,
  placeholder,
  password,
  iconRight,
  iconLabelStart,
  iconLabelEnd,
  name,
  value,
  onChange,
}: InputProps) => {
  const baseStyles = "rounded-[15px] border-1 border-secondary-500";

  return (
    <div>
      <div className="flex items-center justify-between mb-2">
        <div className="flex items-center gap-3">
          {iconLabelStart && (
            <span className=" inline-block">{iconLabelStart}</span>
          )}
          {label && (
            <label
              htmlFor={label}
              className="inline-block first-letter:uppercase"
            >
              {label}
            </label>
          )}
        </div>
        {iconLabelEnd && <span>{iconLabelEnd}</span>}
      </div>

      {password ? (
        <InputAntd.Password
          placeholder={placeholder}
          size={size}
          className={baseStyles}
          value={value}
          onChange={onChange}
          name={name}
        />
      ) : (
        <InputAntd
          placeholder={placeholder}
          size={size}
          className={baseStyles}
          suffix={iconRight}
          value={value}
          onChange={onChange}
          name={name}
        />
      )}
    </div>
  );
};
