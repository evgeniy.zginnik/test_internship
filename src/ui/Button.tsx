import { Button as AntdButton } from "antd";

interface ButtonProps {
  label?: string;

  type?: "primary" | "secondary" | "outlined" | "filled";
  size?: "default" | "mini" | "smallest";
  width?: "min" | "fixed" | "fixedSmall" | "max";

  disabled?: boolean;
  className?: string;

  htmlType?: "button" | "submit" | "reset";

  onClick?: () => void;
}

const types = {
  primary: "text-white-100 bg-primary-500",
  secondary: "text-gray-500 bg-secondary-500",
  outlined: "text-gray-500 bg-white-100 border-2 border-gray-700",
  filled: "text-white-100 bg-gray-500 border-2 border-gray-700",
};

const sizes = {
  default: "h-12 text-base rounded-[15px] font-medium",
  mini: "h-9 text-sm rounded-2xl",
  smallest: "h-8 text-xs rounded-xl",
};

const widths = {
  min: "px-11",
  fixed: "w-[343px]",
  fixedSmall: "w-32",
  max: "w-full",
};

export const Button = ({
  label = "Button",
  type = "primary",
  size = "default",
  width = "fixed",
  disabled = false,
  className = "",
  htmlType = "button",
  onClick,
}: ButtonProps) => {
  return (
    <>
      <AntdButton
        type={type !== "filled" ? "primary" : "default"}
        disabled={disabled}
        htmlType={htmlType}
        onClick={onClick}
        className={`${types[type]} ${sizes[size]} ${widths[width]} ${className} flex items-center justify-center fill-gray-500`}
      >
        {label}
      </AntdButton>
    </>
  );
};
