import { Button as AntdButton } from "antd";

interface ButtonProps {
  icon: React.ReactNode;
  color?: "white" | "secondary";
  active?: boolean;
}

const colors = {
  white: "bg-white-700",
  secondary: "bg-secondary-500",
};

export const IconButton = ({
  icon,
  color = "white",
  active = false,
}: ButtonProps) => {
  return (
    <>
      <AntdButton
        type="primary"
        shape="circle"
        icon={icon}
        className={` ${
          active
            ? "bg-gray-500 fill-white-100"
            : `${colors[color]} fill-gray-500`
        } flex justify-center items-center h-9`}
      />
    </>
  );
};
