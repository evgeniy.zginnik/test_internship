import {
  EmailIcon,
  FacebookIcon,
  InstagramIcon,
  LInkedinIcon,
  PinterestIcon,
  TwitterIcon,
} from "../assets/icons/index";
import { IconButton } from "./IconButton";
import CompanyLogo from "../assets/runway-logo.png";

const firstColumn = [
  "Menu",
  "Profile",
  "Save",
  "Compare",
  "Cell now",
  "Search",
  "Articles",
  "Favourite products",
];

const secondColumn = [
  "Aircraft",
  "Parts/products",
  "Real estate",
  "Services",
  "Business aircraft",
  "Company dealer",
  "Features",
  "Engines",
];

const thirdColumn = ["Help", "Security", "Privacy"];

const columns = [firstColumn, secondColumn, thirdColumn];

const icons = [
  <InstagramIcon />,
  <EmailIcon />,
  <PinterestIcon />,
  <TwitterIcon />,
  <LInkedinIcon />,
  <FacebookIcon />,
];

export const Footer = () => {
  return (
    <div className="py-[37px] px-[16px] lg:py-[46px] sm:px-[32px] md:px-[64px] lg:px-[166px] bg-gray-500 text-[#fff] text-[12px] font-light">
      <div className="mb-[38px] flex gap-[25px] md:gap-[60px] justify-center">
        {icons.map((icon, i) => (
          <IconButton key={i} icon={icon} />
        ))}
      </div>
      <div className="flex justify-between mb-[41px]">
        {columns.map((column, i) => (
          <ul key={i} className="flex flex-col gap-4">
            {column.map((title) => (
              <li key={title}>{title}</li>
            ))}
          </ul>
        ))}
      </div>
      <div className="flex justify-between">
        <div>
          <p>Runway5</p>
          <p>Copyright &copy; 2023</p>
        </div>
        <div>
          <img src={CompanyLogo} alt="logo" />
        </div>
      </div>
    </div>
  );
};
