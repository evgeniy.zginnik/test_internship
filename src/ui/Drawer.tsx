import { ComponentType, FC, useState } from "react";
import { Drawer as DrawerAntd } from "antd";
import { CloseSubFormIcon } from "../assets/icons";

interface DrawerProps {
  firstChildTitle: string;
  secondChildTitle: string;
  open: boolean;
  onClose: () => void;
  width: number;
  firstChild: ComponentType<{ onRegistrationChose: () => void }>;
  secondChild?: ComponentType<{ onCloseForm: () => void }>;
  firstChildExtra: React.ReactNode;
}

export const Drawer: FC<DrawerProps> = ({
  firstChildTitle,
  secondChildTitle,
  open,
  onClose,
  width = 413,
  firstChild: FirstChild,
  secondChild: SecondChild,
  firstChildExtra,
}) => {
  const [childrenDrawer, setChildrenDrawer] = useState(false);

  const showChildrenDrawer = () => {
    setChildrenDrawer(true);
  };

  const closeChildrenDrawer = () => {
    setChildrenDrawer(false);
  };

  const handleCloseAllDrawers = () => {
    setChildrenDrawer(false);

    onClose();
  };

  const childrenPosition = childrenDrawer ? `${width}px` : 0;

  return (
    <>
      <DrawerAntd
        title={secondChildTitle}
        width={width}
        closable={false}
        onClose={handleCloseAllDrawers}
        open={open}
        className=""
        contentWrapperStyle={{
          right: childrenPosition,
        }}
        extra={
          <CloseSubFormIcon
            onClick={closeChildrenDrawer}
            className="cursor-pointer"
          />
        }
      >
        {SecondChild && <SecondChild onCloseForm={closeChildrenDrawer} />}
      </DrawerAntd>
      <DrawerAntd
        title={firstChildTitle}
        width={width}
        closable={false}
        open={open}
        className=""
        contentWrapperStyle={{}}
        mask={false}
        extra={firstChildExtra}
      >
        <FirstChild onRegistrationChose={showChildrenDrawer} />
      </DrawerAntd>
    </>
  );
};
