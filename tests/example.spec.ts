import { test, expect } from "@playwright/test";

const devUrl = "http://localhost:5173/";

/**

 page.locator('.selector') - choose element
 page.locator('.selector').fill(test@mail.com) - fill input
 */

// test("has title", async ({ page }) => {
//   await page.goto("https://google.com/%22);

//   // Expect a title "to contain" a substring.
//   await expect(page).toHaveTitle(/Google/);
// });

// test("get started link", async ({ page }) => {
//   await page.goto(devUrl);

//   // Click the get started link.
//   await page.getByRole("link", { name: "Get started" }).click();

//   // Expects the URL to contain intro.
//   await expect(page).toHaveURL(/.*intro/);
// });

test("button should be disabled", async ({ page }) => {
  await page.goto(devUrl);

  await expect(
    await page.getByRole("button", { name: "disabled" })
  ).toBeDisabled();
});
